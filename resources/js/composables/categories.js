import { ref, inject } from 'vue';
import { useRouter } from 'vue-router';

export default function useCategories() {
    const categories = ref([]);
    const categoryList = ref([]);
    const category = ref({ name: '' });
    const router = useRouter();
    const validationErrors = ref({});
    const isLoading = ref(false);
    const swal = inject('$swal');

    const axiosConfig = {
        handleSuccess(response) {
            return response.data;
        },
        handleFailure(error) {
            if (error.response?.data) {
                validationErrors.value = error.response.data.errors;
            }
        },
    };

    const api = {
        get(url, config = {}) {
            return axios.get(url, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
        post(url, data, config = {}) {
            return axios.post(url, data, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
        put(url, data, config = {}) {
            return axios.put(url, data, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
        delete(url, config = {}) {
            return axios.delete(url, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
    };

    const getCategories = async (
        page = 1,
        search_id = '',
        search_title = '',
        search_global = '',
        order_column = 'created_at',
        order_direction = 'desc'
    ) => {
        const response = await api.get(`/api/categories?page=${page}&search_id=${search_id}&search_title=${search_title}&search_global=${search_global}&order_column=${order_column}&order_direction=${order_direction}`);
        categories.value = response;
    };

    const getCategory = async (id) => {
        const response = await api.get(`/api/categories/${id}`);
        category.value = response.data.data;
    };

    const storeOrUpdateCategory = async (category, isUpdate = false) => {
        if (isLoading.value) return;

        isLoading.value = true;
        validationErrors.value = {};

        const apiMethod = isUpdate ? api.put : api.post;
        const url = isUpdate ? `/api/categories/${category.id}` : '/api/categories';

        apiMethod(url, category)
            .then(() => {
                router.push({ name: 'categories.index' });
                const successMessage = isUpdate ? 'Category updated successfully' : 'Category saved successfully';
                swal({ icon: 'success', title: successMessage });
            })
            .finally(() => isLoading.value = false);
    };

    const deleteCategory = async (id) => {
        const result = await swal({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this action!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            confirmButtonColor: '#ef4444',
            timer: 20000,
            timerProgressBar: true,
            reverseButtons: true
        });

        if (result.isConfirmed) {
            await api.delete(`/api/categories/${id}`);
            getCategories();
            router.push({ name: 'categories.index' });
            swal({ icon: 'success', title: 'Category deleted successfully' });
        }
    };

    const getCategoryList = async () => {
        const response = await api.get('/api/category-list');
        categoryList.value = response.data.data;
    };

    return {
        categoryList,
        categories,
        category,
        getCategories,
        getCategoryList,
        getCategory,
        storeOrUpdateCategory,
        deleteCategory,
        validationErrors,
        isLoading
    };
}
