import { ref, inject } from 'vue';
import { useRouter } from 'vue-router';

export default function useBoards() {
    const boards = ref([]);
    const boardList = ref([]);
    const board = ref({ title: '' });
    const router = useRouter();
    const validationErrors = ref({});
    const isLoading = ref(false);
    const swal = inject('$swal');

    const axiosConfig = {
        handleSuccess(response) {
            return response.data;
        },
        handleFailure(error) {
            if (error.response?.data) {
                validationErrors.value = error.response.data.errors;
            }
        },
    };

    const api = {
        get(url, config = {}) {
            return axios.get(url, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
        post(url, data, config = {}) {
            return axios.post(url, data, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
        put(url, data, config = {}) {
            return axios.put(url, data, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
        delete(url, config = {}) {
            return axios.delete(url, config)
                .then(axiosConfig.handleSuccess)
                .catch(axiosConfig.handleFailure);
        },
    };

    const getBoards = async (
        page = 1,
        search_id = '',
        search_title = '',
        search_global = '',
        order_column = 'created_at',
        order_direction = 'desc'
    ) => {
        const response = await api.get(`/api/boards?page=${page}&search_id=${search_id}&search_title=${search_title}&search_global=${search_global}&order_column=${order_column}&order_direction=${order_direction}`);
        boards.value = response;
    };

    const getBoard = async (id) => {
        const response = await api.get(`/api/boards/${id}`);
        board.value = response.data;
    };

    const storeOrUpdateBoard = async (board, isUpdate = false) => {
        if (isLoading.value) return;

        isLoading.value = true;
        validationErrors.value = {};

        const apiMethod = isUpdate ? api.put : api.post;
        const url = isUpdate ? `/api/boards/${board.id}` : '/api/boards';

        apiMethod(url, board)
            .then(() => {
                router.push({ name: 'boards.index' });
                const successMessage = isUpdate ? 'Board updated successfully' : 'Board saved successfully';
                swal({ icon: 'success', title: successMessage });
            })
            .finally(() => isLoading.value = false);
    };

    const deleteBoard = async (id) => {
        const result = await swal({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this action!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            confirmButtonColor: '#ef4444',
            timer: 20000,
            timerProgressBar: true,
            reverseButtons: true
        });

        if (result.isConfirmed) {
            await api.delete(`/api/boards/${id}`);
            getBoards();
            router.push({ name: 'boards.index' });
            swal({ icon: 'success', title: 'Board deleted successfully' });
        }
    };

    const getBoardList = async () => {
        const response = await api.get('/api/board-list');
        boardList.value = response.data.data;
    };

    return {
        boardList,
        boards,
        board,
        getBoards,
        getBoardList,
        getBoard,
        storeOrUpdateBoard,
        deleteBoard,
        validationErrors,
        isLoading
    };
}
