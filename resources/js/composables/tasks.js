import {ref, inject} from 'vue'
import {useRouter} from 'vue-router'
import store from "../store";

export default function useTasks() {
    const tasks = ref({})
    const task = ref({
        title: '',
        content: '',
        category_id: '',
        thumbnail: '',
        assignee_id: ''
    })
    const router = useRouter()
    const validationErrors = ref({})
    const isLoading = ref(false)
    const swal = inject('$swal')

    const getTasks = async (
        page = 1,
        search_category = '',
        search_id = '',
        search_title = '',
        search_content = '',
        search_global = '',
        order_column = 'created_at',
        order_direction = 'desc'
    ) => {
        axios.get('/api/tasks?board_id=' + store.state.space.board +
            '&page=' + page +
            '&search_category=' + search_category +
            '&search_id=' + search_id +
            '&search_title=' + search_title +
            '&search_content=' + search_content +
            '&search_global=' + search_global +
            '&order_column=' + order_column +
            '&order_direction=' + order_direction)
            .then(response => {
                tasks.value = response.data;
            })
    }

    const getTask = async (id) => {
        axios.get('/api/tasks/' + id)
            .then(response => {
                task.value = response.data.data;
            })
    }

    const storeTask = async (task) => {
        if (isLoading.value) return;

        isLoading.value = true
        validationErrors.value = {}

        let serializedTask = new FormData()
        for (let item in task) {
            if (task.hasOwnProperty(item)) {
                serializedTask.append(item, task[item])
            }
        }

        axios.post('/api/tasks', serializedTask, {
            headers: {
                "content-type": "multipart/form-data"
            }
        })
            .then(response => {
                router.push({name: 'tasks.index'})
                swal({
                    icon: 'success',
                    title: 'Task saved successfully'
                })
            })
            .catch(error => {
                if (error.response?.data) {
                    validationErrors.value = error.response.data.errors
                }
            })
            .finally(() => isLoading.value = false)
    }

    const updateTask = async (task) => {
        if (isLoading.value) return;

        isLoading.value = true
        validationErrors.value = {}

        axios.put('/api/tasks/' + task.id, task)
            .then(response => {
                router.push({name: 'tasks.index'})
                swal({
                    icon: 'success',
                    title: 'Task updated successfully'
                })
            })
            .catch(error => {
                if (error.response?.data) {
                    validationErrors.value = error.response.data.errors
                }
            })
            .finally(() => isLoading.value = false)
    }

    const deleteTask = async (id) => {
        swal({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this action!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            confirmButtonColor: '#ef4444',
            timer: 20000,
            timerProgressBar: true,
            reverseButtons: true
        })
            .then(result => {
                if (result.isConfirmed) {
                    axios.delete('/api/tasks/' + id)
                        .then(response => {
                            getTasks()
                            router.push({name: 'tasks.index'})
                            swal({
                                icon: 'success',
                                title: 'Task deleted successfully'
                            })
                        })
                        .catch(error => {
                            swal({
                                icon: 'error',
                                title: 'Something went wrong'
                            })
                        })
                }
            })

    }

    return {
        tasks,
        task,
        getTasks,
        getTask,
        storeTask,
        updateTask,
        deleteTask,
        validationErrors,
        isLoading
    }
}
