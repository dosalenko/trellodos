import Cookies from 'js-cookie'

export default {
    namespaced: true,
    state: {
        space: null
    },
    getters: {
        space: state => state.space
    },
    mutations: {
        SET_SPACE(state, space) {
            state.board = space
        }
    },
    actions: {
        setSpace({ commit }, space) {
            commit('SET_SPACE', space)
        }
    }
}
