import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from '../store/auth'
import lang from '../store/lang'
import space from '../store/space'

const store = createStore({
    plugins:[
        createPersistedState()
    ],
    modules:{
        auth,
        lang,
        space,
    },
    mutations: {
        CREATE_TASK(state, { tasks, name }) {
            tasks.push({
                name,
                id: uuidv4(),
                description: ''
            })
        },
        CREATE_COLUMN(state, { name }) {
            state.board.columns.push({
                name,
                tasks: []
            })
        },
        UPDATE_TASK(state, { task, key, value }) {
            task[key] = value
        },
        MOVE_TASK(state, { fromColumnTasks, fromTaskIndex, toColumnTasks, toTaskIndex }) {
            console.log(toColumnTasks)
            const taskToMove = fromColumnTasks.splice(fromTaskIndex, 1)[0]
            toColumnTasks.splice(toTaskIndex, 0, taskToMove)
        },
        MOVE_COLUMN(state, { fromColumnIndex, toColumnIndex }) {
            const columnList = state.board.columns
            const columnToMove = columnList.splice(fromColumnIndex, 1)[0]
            columnList.splice(toColumnIndex, 0, columnToMove)
        }
    },
})

export default store
