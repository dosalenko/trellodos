import store from "../store";

const AuthenticatedLayout = () => import('../layouts/Authenticated.vue')
const GuestLayout = () => import('../layouts/Guest.vue');

const TasksIndex = () => import('../views/admin/tasks/Index.vue');
const TasksCreate = () => import('../views/admin/tasks/Create.vue');
const TasksEdit = () => import('../views/admin/tasks/Edit.vue');

function requireLogin(to, from, next) {
    let isLogin = false;
    isLogin = !!store.state.auth.authenticated;
    if (isLogin) {
        next()
    } else {
        next('/login')
    }
}

function isAdmin(to, from, next) {
    if (store.state.auth.user.created_by === null) {
        next()
    } else {
        next('/boards')
    }
}

function guest(to, from, next) {
    let isLogin;
    isLogin = !!store.state.auth.authenticated;

    if (isLogin) {
        next('/')
    } else {
        next()
    }
}

export default [
    {
        path: '/',
        component: GuestLayout,
        children: [
            {
                path: '/',
                name: 'home',
                component: () => import('../views/home/index.vue'),
            },
            {
                path: 'boards',
                name: 'public-boards.index',
                component: () => import('../views/boards/index.vue'),
                beforeEnter: requireLogin,
            },
            {
                path: 'boards/:id',
                name: 'public-boards.details',
                component: () => import('../views/boards/details.vue'),
                beforeEnter: requireLogin,
            },
            {
                path: 'tasks',
                name: 'public-tasks.index',
                component: () => import('../views/tasks/index.vue'),
            },
            {
                path: 'tasks/:id',
                name: 'public-tasks.details',
                component: () => import('../views/tasks/details.vue'),
            },
            {
                path: 'category/:id',
                name: 'category-tasks.index',
                component: () => import('../views/category/tasks.vue'),
            },
            {
                path: 'login',
                name: 'auth.login',
                component: () => import('../views/login/Login.vue'),
                beforeEnter: guest,
            },
            {
                path: 'register',
                name: 'auth.register',
                component: () => import('../views/register/index.vue'),
                beforeEnter: guest,
            },
            {
                path: 'forgot-password',
                name: 'auth.forgot-password',
                component: () => import('../views/auth/passwords/Email.vue'),
                beforeEnter: guest,
            },
            {
                path: 'reset-password/:token',
                name: 'auth.reset-password',
                component: () => import('../views/auth/passwords/Reset.vue'),
                beforeEnter: guest,
            },
        ]
    },
    {
        path: '/admin',
        component: AuthenticatedLayout,
        beforeEnter: requireLogin && isAdmin,
        children: [
            {
                name: 'admin.index',
                path: '',
                component: () => import('../views/admin/index.vue'),
                meta: {breadCrumb: 'Admin'}
            },
            {
                name: 'boards.index',
                path: 'boards',
                component: () => import('../views/admin/boards/Index.vue'),
                meta: {breadCrumb: 'Boards'}
            },
            {
                name: 'boards.create',
                path: 'boards/create',
                component: () => import('../views/admin/boards/Create.vue'),
                meta: {breadCrumb: 'Add new board'}
            },
            {
                name: 'boards.edit',
                path: 'boards/edit/:id',
                component: () => import('../views/admin/boards/Edit.vue'),
                meta: {breadCrumb: 'Edit Board'}
            },
            {
                name: 'profile.index',
                path: 'profile',
                component: () => import('../views/admin/profile/index.vue'),
                meta: {breadCrumb: 'Profile'}
            },
            {
                name: 'tasks.index',
                path: 'tasks',
                component: TasksIndex,
                meta: {breadCrumb: 'Tasks'}
            },
            {
                name: 'tasks.create',
                path: 'tasks/create',
                component: TasksCreate,
                meta: {breadCrumb: 'Add new task'}
            },
            {
                name: 'tasks.edit',
                path: 'tasks/edit/:id',
                component: TasksEdit,
                meta: {breadCrumb: 'Edit task'}
            },
            {
                name: 'categories.index',
                path: 'categories',
                component: () => import('../views/admin/categories/Index.vue'),
                meta: {breadCrumb: 'Categories'}
            },
            {
                name: 'categories.create',
                path: 'categories/create',
                component: () => import('../views/admin/categories/Create.vue'),
                meta: {breadCrumb: 'Add new category'}
            },
            {
                name: 'categories.edit',
                path: 'categories/edit/:id',
                component: () => import('../views/admin/categories/Edit.vue'),
                meta: {breadCrumb: 'Edit Category'}
            },
            {
                name: 'permissions.index',
                path: 'permissions',
                component: () => import('../views/admin/permissions/Index.vue'),
                meta: {breadCrumb: 'Permissions'}
            },
            {
                name: 'permissions.create',
                path: 'permissions/create',
                component: () => import('../views/admin/permissions/Create.vue'),
                meta: {breadCrumb: 'Create Permission'}
            },
            {
                name: 'permissions.edit',
                path: 'permissions/edit/:id',
                component: () => import('../views/admin/permissions/Edit.vue'),
                meta: {breadCrumb: 'Permission Edit'}
            },
            {
                name: 'roles.index',
                path: 'roles',
                component: () => import('../views/admin/roles/Index.vue'),
                meta: {breadCrumb: 'Roles'}
            },
            {
                name: 'roles.create',
                path: 'roles/create',
                component: () => import('../views/admin/roles/Create.vue'),
                meta: {breadCrumb: 'Create Role'}
            },
            {
                name: 'roles.edit',
                path: 'roles/edit/:id',
                component: () => import('../views/admin/roles/Edit.vue'),
                meta: {breadCrumb: 'Role Edit'}
            },
            {
                name: 'users.index',
                path: 'users',
                component: () => import('../views/admin/users/Index.vue'),
                meta: {breadCrumb: 'Users'}
            },
            {
                name: 'users.create',
                path: 'users/create',
                component: () => import('../views/admin/users/Create.vue'),
                meta: {breadCrumb: 'Add New'}
            },
            {
                name: 'users.edit',
                path: 'users/edit/:id',
                component: () => import('../views/admin/users/Edit.vue'),
                meta: {breadCrumb: 'User Edit'}
            },
            {
                path: 'payment/checkout',
                name: 'payment',
                component: () => import('../views/admin/payments/PaymentForm.vue'),
                beforeEnter: requireLogin, // You can add any necessary guards here
            },
        ]
    },
    {
        path: "/:pathMatch(.*)*",
        name: 'NotFound',
        component: () => import("../views/errors/404.vue"),
    },
];
