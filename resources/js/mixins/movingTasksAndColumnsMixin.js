export default {
    props: {
        board: {
            type: Object,
            required: true
        },
        task: {
            type: Object,
            required: true
        },
        column: {
            type: Object,
            required: true
        },
        columnIndex: {
            type: Number,
            required: true
        }
    },
    methods: {
        moveTaskOrColumn(event, toColumnTasks, toColumnIndex, toTaskIndex) {
            const type = event.dataTransfer.getData('type')

            if (type === 'task') {
                this.moveTask(event, toColumnTasks, toTaskIndex !== undefined ? toTaskIndex : toColumnTasks.length)
            } else {
                this.moveColumn(event, toColumnIndex)
            }
        },
        moveTask(event, toColumnTasks, toTaskIndex) {

            const fromColumnIndex = event.dataTransfer.getData('from-column-index')
            const fromTaskIndex = event.dataTransfer.getData('from-task-index')
            const fromColumnTasks = this.board.columns[fromColumnIndex].tasks

            const taskId = fromColumnTasks[fromTaskIndex].id;
            const columnId = this.board.columns;

            axios.post('/api/task/status', {task: taskId, category: this.findMovedCategoryId(toColumnTasks)})
                .then(response => {
//
                })
                .catch(error => {
                    console.log(errors);
                })

            this.$store.commit('MOVE_TASK', {
                fromColumnTasks,
                fromTaskIndex,
                toColumnTasks,
                toTaskIndex
            })
        },
        moveColumn(event, toColumnIndex) {
            const fromColumnIndex = event.dataTransfer.getData('from-column-index')

            this.$store.commit('MOVE_COLUMN', {
                fromColumnIndex,
                toColumnIndex
            })
        },

        findMovedCategoryId(categoryArray) {
            for (let i = 0; i < categoryArray.length; i++) {
                const category = categoryArray[i];
                if (category.category_id !== undefined) {
                    return category.category_id;
                }
            }
            return undefined;
        }

    }
}
