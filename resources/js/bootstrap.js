import 'bootstrap';
import axios from 'axios';
import { io } from 'socket.io-client';
import _ from 'lodash'; // Import lodash

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.withCredentials = true;

window.io = io;
window._ = _;

import Echo from 'laravel-echo';

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':3000',
});
