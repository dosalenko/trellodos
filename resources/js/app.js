import './bootstrap';

import { createApp } from 'vue';
import BootstrapVueNext from 'bootstrap-vue-next'
import { Bootstrap5Pagination } from 'laravel-vue-pagination';
import store from './store'
import router from './routes/index'
import VueSweetalert2 from "vue-sweetalert2";
import { abilitiesPlugin } from '@casl/vue';
import ability from './services/ability';
import vSelect from "vue-select";
import useAuth from './composables/auth';
import i18n from "./plugins/i18n";
import PaymentForm from './views/admin/payments/PaymentForm.vue';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css'
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vue-select/dist/vue-select.css';

const app = createApp({
    created() {
        useAuth().getUser()
    }
});


app.use(router)
app.use(store)
app.use(BootstrapVueNext)
app.use(VueSweetalert2)
app.use(i18n)
app.use(abilitiesPlugin, ability)
app.component('Pagination', Bootstrap5Pagination)
app.component("v-select", vSelect);
app.component('PaymentForm', PaymentForm);
app.mount('#app')
