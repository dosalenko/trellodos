<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout</title>
    <script src="https://js.stripe.com/v3/"></script>
</head>
<body>
<form id="payment-form">
    <label for="payment-method">Payment Method:</label>
    <select id="payment-method" name="payment-method">
        <option value="card">Credit Card</option>
        <option value="iban">IBAN</option>
        @foreach ($paymentMethods as $paymentMethod)
            <option value="{{ $paymentMethod->id }}">{{ Cashier::cardBrand($paymentMethod->card_brand) }} ending in {{ $paymentMethod->card_last_four }}</option>
        @endforeach
    </select>

    <div id="card-element" class="stripe-element"></div>
    <div id="iban-element" class="stripe-element" style="display:none;"></div>
    <!-- Add more div elements for additional payment methods -->

    <button id="submit">
        <div class="spinner hidden" id="spinner"></div>
        <span id="button-text">Pay Now</span>
    </button>
    <p id="card-error" role="alert"></p>
</form>

<script>
    var stripe = Stripe('{{ config('cashier.key') }}');
    var elements = stripe.elements();
    var card = elements.create('card');
    var iban = elements.create('iban', {
        supportedCountries: ['SEPA'],
    });
    // Add more elements for additional payment methods

    card.mount('#card-element');
    iban.mount('#iban-element');
    // Mount additional elements for other payment methods

    var paymentMethodSelect = document.getElementById('payment-method');
    paymentMethodSelect.addEventListener('change', function () {
        var selectedMethod = paymentMethodSelect.value;

        document.getElementById('card-element').style.display = selectedMethod === 'card' ? 'block' : 'none';
        document.getElementById('iban-element').style.display = selectedMethod === 'iban' ? 'block' : 'none';
        // Display additional elements for other payment methods based on selection
    });

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        var selectedMethod = paymentMethodSelect.value;
        var stripeElement;

        switch (selectedMethod) {
            case 'card':
                stripeElement = card;
                break;
            case 'iban':
                stripeElement = iban;
                break;
            // Add cases for additional payment methods

            default:
                stripeElement = card; // Default to card
                break;
        }

        stripe.createPaymentMethod(selectedMethod, stripeElement).then(function(result) {
            if (result.error) {
                errorElement.textContent = result.error.message;
            } else {
                // Handle payment success
                // Send payment method ID to your server
            }
        });
    });
</script>
</body>
</html>
