const http = require('http');
const express = require('express');
const socketIo = require('socket.io');
const redis = require('socket.io-redis');

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server, {
    cors: { origin: "*" }
});

app.get('/', (req, res) => {
    res.send('Socket.io server is running!');
});

io.adapter(redis({ host: process.env.REDIS_HOST || 'localhost', port: process.env.REDIS_PORT || 6379 }));

io.on('connection', (socket) => {
    console.log('A user connected');

    socket.on('joinBoard', (boardId) => {
        socket.join(boardId);
        console.log(`User joined board: ${boardId}`);
    });

    socket.on('createCard', (data) => {
        console.log(`New card created on board ${data.boardId}`);
        console.log(data.card);

        io.to(data.boardId).emit('cardCreated', {
            card: data.card,
        });
    });

    socket.on('disconnect', () => {
        console.log('User disconnected');
    });
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log(`Socket.io server is listening on *:${PORT}`);
});
