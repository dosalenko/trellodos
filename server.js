const http = require('http');
const express = require('express');
const socketIo = require('socket.io');
const redis = require('redis');
const socketIoAuth = require('socketio-auth');

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
    cors: { origin: "*" }
});

const REDIS_HOST = 'localhost';
const REDIS_PORT = 6379;

const redisClient = redis.createClient({
    host: REDIS_HOST,
    port: REDIS_PORT
});

// Subscribe to channels
redisClient.subscribe('create_task_channel');
redisClient.subscribe('change_task_status_channel');

// Handle messages from Redis
redisClient.on('message', (channel, message) => {
    console.log(`Received message from channel '${channel}': ${message}`);

    try {
        const parsedMessage = JSON.parse(message);

        if (parsedMessage && parsedMessage.event && parsedMessage.data) {
            const { event, data } = parsedMessage;

            io.sockets.emit(event, data);
        } else {
            console.error('Invalid message structure:', message);
        }
    } catch (error) {
        console.error('Error parsing message:', error);
    }
});

// Authentication logic using socket.io-auth middleware
socketIoAuth(io, {
    authenticate: (socket, data, callback) => {
        // You can implement your own authentication logic here
        const isAuthenticated = /* your authentication logic */ true;

        if (isAuthenticated) {
            return callback(null, true);
        }

        return callback(new Error('Authentication failed'), false);
    },
    postAuthenticate: (socket) => {
        console.log(`User authenticated: ${socket.id}`);
    },
    disconnect: (socket) => {
        console.log(`User disconnected: ${socket.id}`);
    },
});

// Handle socket.io connections
io.on('connection', (socket) => {
    console.log(`User connected: ${socket.id}`);

    socket.on('disconnect', () => {
        console.log(`User disconnected: ${socket.id}`);
    });
});

const PORT = 3000;
server.listen(PORT, () => {
    console.log(`Socket.io server is listening on *:${PORT}`);
});
