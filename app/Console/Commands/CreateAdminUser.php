<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUser extends Command
{
    protected $signature = 'admin:create';
    protected $description = 'Create a new admin user';

    public function handle()
    {
        $name = $this->ask('Enter the name for the admin user');
        $email = $this->askValidEmail('Enter the email for the admin user');
        $password = $this->secret('Enter the password for the admin user');
        $confirmPassword = $this->secret('Confirm the password');

        $this->validatePasswordConfirmation($password, $confirmPassword);

        if ($this->isEmailUnique($email)) {
            $user = User::create([
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password)
            ]);

            $role = Role::firstOrCreate(['name' => 'admin']);

            $permissions = Permission::pluck('id', 'id')->all();

            $role->syncPermissions($permissions);

            $user->assignRole([$role->id]);

            $this->info('Admin user created successfully!');
        } else {
            $this->error('Email already exists. Please enter a unique email.');
            $this->handle();
        }
    }

    protected function askValidEmail($question)
    {
        $email = $this->ask($question);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Invalid email format. Please enter a valid email.');
            return $this->askValidEmail($question);
        }

        return $email;
    }

    protected function validatePasswordConfirmation($password, $confirmPassword)
    {
        if ($password !== $confirmPassword) {
            $this->error('Password and confirmation do not match. Please try again.');
            $this->handle();
        }
    }

    protected function isEmailUnique($email)
    {
        return User::where('email', $email)->doesntExist();
    }
}
