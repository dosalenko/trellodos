<?php

namespace App\Providers;

use App\Events\CardCreated;
use App\Events\PaymentFailed;
use App\Events\PaymentSucceeded;
use App\Listeners\CardCreatedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        PaymentSucceeded::class => [
            \App\Listeners\HandlePaymentSucceeded::class,
        ],

        PaymentFailed::class => [
            \App\Listeners\HandlePaymentFailed::class,
        ],

        CardCreated::class => [
            CardCreatedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
