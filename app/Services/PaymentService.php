<?php

namespace App\Services;

use App\Models\PaymentHistory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Stripe\PaymentIntent;

class PaymentService
{
    /*
     *
     */
    public function createPaymentIntent($amount)
    {
        try {
            $paymentIntent = PaymentIntent::create([
                'amount' => $amount,
                'currency' => 'usd',
            ]);

            PaymentHistory::create([
                'user_id' => Auth::user()->id,
                'payment_intent_id' => $paymentIntent->id,
                'amount' => $amount,
            ]);

            return $paymentIntent->client_secret;
        } catch (\Exception $e) {
            Log::error('PaymentService: ' . $e->getMessage());
            throw $e;
        }
    }

    /*
     *
     */
    public function retrievePaymentMethods($user)
    {
        try {
            $user = auth()->user();

            $paymentMethods = \Stripe\PaymentMethod::all([
                'customer' => $user->stripe_id,
                'type' => 'card',
            ]);

            return response()->json(['paymentMethods' => $paymentMethods]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
