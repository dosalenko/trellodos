<?php

namespace App\Http\Resources;

use Exception;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        try {
            $resized_image = $this->getMedia('*')[0]->getUrl('resized-image');
        } catch (Exception $e) {
            $resized_image = "";
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'categories' => $this->categories,
            'content_excerpt' => substr($this->content, 0, 50) . '...',
            'content' => $this->content,
            'original_image' => count($this->getMedia('*')) > 0 ? $this->getMedia('*')[0]->getUrl() : null,
            'resized_image' => $resized_image,
            'created_at' => $this->created_at->toDateString(),
            'assignee_name' => $this->assignee ? $this->assignee->name : null,
            'assignee_id' => $this->assignee_id
        ];
    }
}
