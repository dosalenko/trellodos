<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * @OA\Get(
     *      path="/categories",
     *      operationId="getCategories",
     *      tags={"Categories"},
     *      summary="Get list of categories",
     *      description="Returns a paginated list of categories with filtering and sorting options",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/CategoryResource")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index()
    {
        $user = Auth::user();

        $orderColumn = request('order_column', 'created_at');
        if (!in_array($orderColumn, ['id', 'name', 'created_at'])) {
            $orderColumn = 'created_at';
        }
        $orderDirection = request('order_direction', 'desc');
        if (!in_array($orderDirection, ['asc', 'desc'])) {
            $orderDirection = 'desc';
        }
        $categories = Category::
        when(request('search_id'), function ($query) {
            $query->where('id', request('search_id'));
        })
            ->when(request('search_title'), function ($query) {
                $query->where('name', 'like', '%' . request('search_title') . '%');
            })
            ->when(request('search_global'), function ($query) {
                $query->where(function ($q) {
                    $q->where('id', request('search_global'))
                        ->orWhere('name', 'like', '%' . request('search_global') . '%');

                });
            })
            ->where('user_id', $user->id)
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(50);
        return CategoryResource::collection($categories);
    }

    /**
     * @OA\Post(
     *      path="/categories",
     *      operationId="storeCategory",
     *      tags={"Categories"},
     *      summary="Create a new category",
     *      description="Create a new category",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreCategoryRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Category created successfully",
     *          @OA\JsonContent(ref="#/components/schemas/CategoryResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreCategoryRequest $request)
    {
        $this->authorize('category-create');
        $category = new Category($request->validated());
        $category->user_id = Auth::user()->id;
        $category->save();

        return new CategoryResource($category);
    }

    /**
     * @OA\Get(
     *      path="/categories/{category}",
     *      operationId="getCategoryById",
     *      tags={"Categories"},
     *      summary="Get a category by ID",
     *      description="Retrieve details of a specific category by ID. Only admins can edit categories, and they can only edit their own categories.",
     *      security={{"sanctum": {}}},
     *      @OA\Parameter(
     *          name="category",
     *          in="path",
     *          description="ID of the category",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CategoryResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Only admins can edit categories, and they can only edit their own categories."
     *      )
     * )
     */
    public function show(Category $category)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            if ($category->user_id === $user->id) {
                return new CategoryResource($category);
            } else {
                return response()->json(['status' => 403, 'success' => false, 'message' => 'You can only edit your own category.'], 403);
            }
        } else {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'You do not have permission to view this category.'], 403);
        }
    }


    /**
     * @OA\Put(
     *      path="/categories/{category}",
     *      operationId="updateCategory",
     *      tags={"Categories"},
     *      summary="Update a category",
     *      description="Update details of a specific category. Only admins can update categories, and they can only update their own categories.",
     *      security={{"sanctum": {}}},
     *      @OA\Parameter(
     *          name="category",
     *          in="path",
     *          description="ID of the category",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreCategoryRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Category updated successfully",
     *          @OA\JsonContent(ref="#/components/schemas/CategoryResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Only admins can update categories, and they can only update their own categories."
     *      )
     * )
     */
    public function update(Category $category, StoreCategoryRequest $request)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $category->update($request->validated());
            return new CategoryResource($category);
        } else {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'You do not have permission to update categories.'], 403);
        }
    }

    /**
     * @OA\Delete(
     *      path="/categories/{category}",
     *      operationId="deleteCategory",
     *      tags={"Categories"},
     *      summary="Delete a category",
     *      description="Delete a specific category. Only admins can delete categories, and they can only delete their own categories.",
     *      security={{"sanctum": {}}},
     *      @OA\Parameter(
     *          name="category",
     *          in="path",
     *          description="ID of the category",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Category deleted successfully"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Only admins can delete categories, and they can only delete their own categories."
     *      )
     * )
     */
    public function destroy(Category $category)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $category->delete();
            return response()->noContent();
        } else {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'You do not have permission to delete categories.'], 403);
        }
    }


    /**
     * @OA\Get(
     *      path="/categories/all",
     *      operationId="getAllCategories",
     *      tags={"Categories"},
     *      summary="Get all categories",
     *      description="Returns a list of all categories",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/CategoryResource")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getList()
    {
        return CategoryResource::collection(Category::where('user_id', auth()->id())->get());
    }
}
