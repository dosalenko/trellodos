<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBoardRequest;
use App\Http\Resources\BoardResource;
use App\Models\Board;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BoardController extends Controller
{
    /**
     * @OA\Get(
     *      path="/boards",
     *      operationId="getBoardsList",
     *      tags={"Boards"},
     *      summary="Get list of boards",
     *      description="Returns a paginated list of boards. Only admins are allowed to access the list, and they can see only their own boards.",
     *      security={{"sanctum": {}}},
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BoardResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Only admins are allowed to access the list of boards."
     *      )
     * )
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $boards = Board::where('user_id', Auth::user()->id)->paginate(100);
            return BoardResource::collection($boards);
        }

        return response()->json(['message' => 'Unauthorized.'], 403);
    }

    /**
     * @OA\Post(
     *      path="/boards",
     *      operationId="storeBoard",
     *      tags={"Boards"},
     *      summary="Create a new board",
     *      description="Create a new board",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreBoardRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Board created successfully",
     *          @OA\JsonContent(ref="#/components/schemas/BoardResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreBoardRequest $request)
    {
        try {
            $this->authorize('board-create');

            $validatedData = $request->validated();
            $validatedData['user_id'] = auth()->id();
            $board = Board::create($validatedData);

            //create a new permission and role set for new board
            $roleName = 'board' . $board->id . '-user';
            $role = Role::create(['name' => $roleName, 'guard_name' => 'web']);

            $permissionName = 'board' . $board->id . '-view';
            $permission = Permission::create(['name' => $permissionName, 'guard_name' => 'web']);
            $role->givePermissionTo($permission);

            return new BoardResource($board);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *      path="/boards/{board}",
     *      operationId="getBoard",
     *      tags={"Boards"},
     *      summary="Retrieve a board",
     *      description="Retrieve details of a specific board. Non-admin users can only view their own boards.",
     *      security={{"sanctum": {}}},
     *      @OA\Parameter(
     *          name="board",
     *          in="path",
     *          description="ID of the board",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BoardResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Non-admin users can only view their own boards."
     *      )
     * )
     */
    public function show(Board $board)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            if ($board->user_id === $user->id) {
                return new BoardResource($board);
            } else {
                return response()->json(['status' => 403, 'success' => false, 'message' => 'You can only edit your own boards.'], 403);
            }
        } else {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'You do not have permission to view this board.'], 403);
        }
    }

    /**
     * @OA\Put(
     *      path="/boards/{board}",
     *      operationId="updateBoard",
     *      tags={"Boards"},
     *      summary="Update a board",
     *      description="Update details of a specific board. Only admins can update boards, and they can update only their own boards.",
     *      @OA\Parameter(
     *          name="board",
     *          in="path",
     *          description="ID of the board",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreBoardRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Board updated successfully",
     *          @OA\JsonContent(ref="#/components/schemas/BoardResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Only admins can update boards, and they can update only their own boards."
     *      )
     * )
     */
    public function update(Board $board, StoreBoardRequest $request)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            if ($board->user_id === $user->id) {
                $board->update($request->validated());
                return new BoardResource($board);
            } else {
                return response()->json(['status' => 403, 'success' => false, 'message' => 'You can only update your own boards.'], 403);
            }
        } else {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'You do not have permission to update boards.'], 403);
        }
    }


    /**
     * @OA\Delete(
     *      path="/boards/{board}",
     *      operationId="deleteBoard",
     *      tags={"Boards"},
     *      summary="Delete a board",
     *      description="Delete a specific board. Only admins can delete boards, and they can delete only their own boards.",
     *      @OA\Parameter(
     *          name="board",
     *          in="path",
     *          description="ID of the board",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Board deleted successfully"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden. Only admins can delete boards, and they can delete only their own boards."
     *      )
     * )
     */
    public function destroy(Board $board)
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            if ($board->user_id === $user->id) {
                $board->delete();
                return response()->noContent();
            } else {
                return response()->json(['status' => 403, 'success' => false, 'message' => 'You can only delete your own boards.'], 403);
            }
        } else {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'You do not have permission to delete boards.'], 403);
        }
    }

    /**
     * @OA\Get(
     *      path="/boards",
     *      operationId="getBoards",
     *      tags={"Boards"},
     *      summary="Get list of boards",
     *      description="Returns a paginated list of boards with categories and media",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/BoardResource")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getBoards()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $boards = Board::where('user_id', $user->id)->latest()->paginate();
        } else {
            $userPermissions = $user->getAllPermissions()->pluck('name')->toArray();

            $boardIds = array_map(function ($permission) {
                return preg_replace('/[^0-9]/', '', $permission);
            }, $userPermissions);

            $boards = Board::whereIn('id', $boardIds)->latest()->paginate();
        }
        return BoardResource::collection($boards);
    }

    /**
     * @OA\Get(
     *      path="/boards/{id}",
     *      operationId="getBoardById",
     *      tags={"Boards"},
     *      summary="Get a board by ID",
     *      description="Retrieve details of a specific board by ID",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the board",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BoardResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getBoard($id)
    {
        $board = Board::with('user', 'tasks', 'tasks.categories')->findOrFail((int)$id);
        $user = Auth::user();

        if ($user->hasPermissionTo("board$id-view") || ($user->hasRole('admin') && $user->id === $board->user_id)) {
            return new BoardResource($board);
        } else {
            return response()->json(['message' => 'Forbidden'], 403);
        }
    }
}
