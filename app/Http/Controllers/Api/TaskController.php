<?php

namespace App\Http\Controllers\Api;

use App\Events\ChangeTaskStatusEvent;
use App\Events\CreateTaskEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Board;
use App\Models\Category;
use App\Models\CategoryTask;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * @OA\Get(
     *      path="/tasks",
     *      operationId="getTasksList",
     *      tags={"Tasks"},
     *      summary="Get list of tasks",
     *      description="Returns a paginated list of tasks with filtering and sorting options",
     *      @OA\Parameter(
     *          name="order_column",
     *          in="query",
     *          description="Column to order by",
     *          required=false,
     *          @OA\Schema(type="string", example="created_at")
     *      ),
     *      @OA\Parameter(
     *          name="order_direction",
     *          in="query",
     *          description="Order direction (asc or desc)",
     *          required=false,
     *          @OA\Schema(type="string", example="desc")
     *      ),
     *      @OA\Parameter(
     *          name="board_id",
     *          in="query",
     *          description="Search by board ID",
     *          required=false,
     *          @OA\Schema(type="integer", example=1)
     *      ),
     *      @OA\Parameter(
     *          name="search_category",
     *          in="query",
     *          description="Filter by category IDs (comma-separated)",
     *          required=false,
     *          @OA\Schema(type="string", example="1,2,3")
     *      ),
     *      @OA\Parameter(
     *          name="search_id",
     *          in="query",
     *          description="Search by task ID",
     *          required=false,
     *          @OA\Schema(type="integer", example=123)
     *      ),
     *      @OA\Parameter(
     *          name="search_title",
     *          in="query",
     *          description="Search by task title",
     *          required=false,
     *          @OA\Schema(type="string", example="Task Title")
     *      ),
     *      @OA\Parameter(
     *          name="search_content",
     *          in="query",
     *          description="Search by task content",
     *          required=false,
     *          @OA\Schema(type="string", example="Task content")
     *      ),
     *      @OA\Parameter(
     *          name="search_global",
     *          in="query",
     *          description="Search globally across ID, title, and content",
     *          required=false,
     *          @OA\Schema(type="string", example="Global search query")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TaskResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index()
    {
        $orderColumn = request('order_column', 'created_at');
        if (!in_array($orderColumn, ['id', 'title', 'created_at'])) {
            $orderColumn = 'created_at';
        }
        $orderDirection = request('order_direction', 'desc');
        if (!in_array($orderDirection, ['asc', 'desc'])) {
            $orderDirection = 'desc';
        }
        $tasks = Task::with('media')
            ->where('board_id', (int)request('board_id'))
            ->where('user_id', Auth::user()->id)
            ->whereHas('categories', function ($query) {
                if (request('search_category')) {
                    $categories = explode(",", request('search_category'));
                    $query->whereIn('id', $categories);
                }
            })
            ->when(request('search_id'), function ($query) {
                $query->where('id', request('search_id'));
            })
            ->when(request('search_title'), function ($query) {
                $query->where('title', 'like', '%' . request('search_title') . '%');
            })
            ->when(request('search_content'), function ($query) {
                $query->where('content', 'like', '%' . request('search_content') . '%');
            })
            ->when(request('search_global'), function ($query) {
                $query->where(function ($q) {
                    $q->where('id', request('search_global'))
                        ->orWhere('title', 'like', '%' . request('search_global') . '%')
                        ->orWhere('content', 'like', '%' . request('search_global') . '%');

                });
            })
            ->when(!auth()->user()->hasPermissionTo('task-all'), function ($query) {
                $query->where('user_id', auth()->id());
            })
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(50);
        return TaskResource::collection($tasks);
    }

    /**
     * @OA\Post(
     *      path="/tasks",
     *      operationId="storeTask",
     *      tags={"Tasks"},
     *      summary="Create a new task",
     *      description="Create a new task with categories and an optional thumbnail",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreTaskRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Task created successfully",
     *          @OA\JsonContent(ref="#/components/schemas/TaskResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreTaskRequest $request)
    {
        $this->authorize('task-create');

        $validatedData = $request->validated();
        $validatedData['user_id'] = auth()->id();

        $task = Task::create($validatedData);

        $board = Board::with('user', 'tasks', 'tasks.categories')->findOrFail((int)$task->board_id);
        event(new CreateTaskEvent($board));

        $categories = explode(",", $request->categories);
        $category = Category::findMany($categories);
        $task->categories()->attach($category);
        try {
            if ($request->hasFile('thumbnail')) {
                $task->addMediaFromRequest('thumbnail')->preservingOriginal()->toMediaCollection('images');
            }
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
        return new TaskResource($task);
    }

    /**
     * @OA\Get(
     *      path="/tasks/{task}",
     *      operationId="getTask",
     *      tags={"Tasks"},
     *      summary="Retrieve a task",
     *      description="Retrieve details of a specific task",
     *      @OA\Parameter(
     *          name="task",
     *          in="path",
     *          description="ID of the task",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TaskResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(Task $task)
    {
        $this->authorize('task-edit');
        if ($task->user_id !== auth()->user()->id && !auth()->user()->hasPermissionTo('task-all')) {
            return response()->json(['status' => 405, 'success' => false, 'message' => 'You can only edit your own tasks']);
        } else {
            return new TaskResource($task);
        }
    }

    /**
     * @OA\Put(
     *      path="/tasks/{task}",
     *      operationId="updateTask",
     *      tags={"Tasks"},
     *      summary="Update a task",
     *      description="Update details of a specific task",
     *      @OA\Parameter(
     *          name="task",
     *          in="path",
     *          description="ID of the task",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreTaskRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Task updated successfully",
     *          @OA\JsonContent(ref="#/components/schemas/TaskResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function update(Task $task, StoreTaskRequest $request)
    {
        $this->authorize('task-edit');
        if ($task->user_id !== auth()->id() && !auth()->user()->hasPermissionTo('task-all')) {
            return response()->json(['status' => 405, 'success' => false, 'message' => 'You can only edit your own tasks']);
        } else {
            $task->update($request->validated());
            $category = Category::findMany($request->categories);
            $task->categories()->sync($category);
            return new TaskResource($task);
        }
    }

    /**
     * @OA\Delete(
     *      path="/tasks/{task}",
     *      operationId="deleteTask",
     *      tags={"Tasks"},
     *      summary="Delete a task",
     *      description="Delete a specific task",
     *      @OA\Parameter(
     *          name="task",
     *          in="path",
     *          description="ID of the task",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Task deleted successfully"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function destroy(Task $task)
    {
        $this->authorize('task-delete');
        if ($task->user_id !== auth()->id() && !auth()->user()->hasPermissionTo('task-all')) {
            return response()->json(['status' => 405, 'success' => false, 'message' => 'You can only delete your own tasks']);
        } else {
            $task->delete();
            return response()->noContent();
        }
    }

    /**
     * @OA\Get(
     *      path="/tasks",
     *      operationId="getTasks",
     *      tags={"Tasks"},
     *      summary="Get list of tasks",
     *      description="Returns a paginated list of tasks with categories and media",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/TaskResource")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getTasks()
    {
        $tasks = Task::with('categories')->with('media')->latest()->paginate();
        return TaskResource::collection($tasks);

    }

    /**
     * Get tasks by category.
     *
     * @OA\Get(
     *      path="/tasks/category/{id}",
     *      operationId="getTasksByCategory",
     *      tags={"Tasks"},
     *      summary="Get tasks by category",
     *      description="Returns tasks related to a specific category",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the category",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/TaskResource")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getCategoryByTasks($id)
    {
        $tasks = Task::whereRelation('categories', 'category_id', '=', $id)->paginate();
        return TaskResource::collection($tasks);
    }

    /**
     * Get a task by ID.
     *
     * @OA\Get(
     *      path="/tasks/{id}",
     *      operationId="getTaskById",
     *      tags={"Tasks"},
     *      summary="Get a task by ID",
     *      description="Retrieve details of a specific task by ID",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the task",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TaskResource")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getTask($id)
    {
        $task = Task::with('categories', 'user', 'media')->findOrFail($id);
        $this->authorize("board{$task->board->id}-view");
        return $task;
    }

    /**
     * Set task status.
     */
    public function setStatus(Request $request)
    {
        $this->authorize('task-edit');

        try {
            $data = $request->all();

            if (isset($data['task']) && isset($data['category'])) {
                return $this->updateTaskCategory((int)$data['task'], (int)$data['category']);
            } else {
                return response()->json(['error' => 'Task and category are required'], 400);
            }
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return response()->json(['error' => 'Internal server error'], 500);
        }
    }

    /**
     * Update task category.
     */
    private function updateTaskCategory($taskId, $categoryId)
    {
        $taskData = Task::findOrFail($taskId);
        $task = CategoryTask::where('task_id', $taskId)->first();

        if ($task) {
            CategoryTask::updateOrInsert(
                ['task_id' => $taskId],
                ['category_id' => $categoryId]
            );

            $board = Board::with('user', 'tasks', 'tasks.categories')->findOrFail($taskData->board_id);
            event(new ChangeTaskStatusEvent($board));

            return response()->json(['message' => 'Task category updated successfully']);
        } else {
            return response()->json(['error' => 'Task not found'], 404);
        }
    }

}
