<?php

namespace App\Http\Controllers;

use App\Models\PaymentHistory;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Stripe\Stripe;
use Stripe\PaymentIntent;
use App\Events\PaymentSucceeded;
use App\Events\PaymentFailed;

class PaymentController extends Controller
{

    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function createPaymentIntent(Request $request)
    {
        try {
            Stripe::setApiKey(config('services.stripe.secret'));

            $client_secret = $this->paymentService->createPaymentIntent((int)$request->get('amount'));

            return ['client_secret' => $client_secret];
        } catch (\Exception $e) {
            Log::error('PaymentService: ' . $e->getMessage());
            throw $e;
        }
    }

    public function getPaymentMethods($user)
    {
        try {
            $user = auth()->user();
            $paymentMethods = $this->paymentService->retrievePaymentMethods($user);

            return response()->json(['paymentMethods' => $paymentMethods]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function handleWebhook(Request $request)
    {
        try {
            $payload = $request->getContent();
            $sigHeader = $request->header('Stripe-Signature');
            $endpointSecret = config('services.stripe.webhook_secret');

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sigHeader, $endpointSecret
            );

            switch ($event->type) {
                case 'payment_intent.succeeded':
                    event(new PaymentSucceeded($event));
                    break;
                case 'payment_intent.payment_failed':
                    event(new PaymentFailed($event));
                    break;
                default:
                    break;
            }

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
