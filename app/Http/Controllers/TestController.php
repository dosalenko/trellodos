<?php

namespace App\Http\Controllers;

use App\Events\CardCreated;
use App\Events\CreateMessageEvent;
use App\Models\PaymentHistory;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Stripe\Stripe;
use Stripe\PaymentIntent;
use App\Events\PaymentSucceeded;
use App\Events\PaymentFailed;
use Illuminate\Support\Facades\Redis;

class TestController extends Controller
{

    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }
}
